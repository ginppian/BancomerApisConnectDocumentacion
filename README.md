BBVA Bancomer Apis Connect
===

<p align="justify">
	Se usan para enrolar al cliente al canal de Bancomer Móvil.
</p>

<p align="justify">
	Las aplicaciones heredan de una clase base <i>BaseViewController</i> cual contiene metodos para llamar, mostrar y configurar los elementos de las APIs.
</p>

<p align="justify">
	Se tiene que cargar una configuracion al inicio de la aplicación por lo general en el método <i>didFinishWithLaunchOptions</i>
</p>

## Estados

1. **ST-BI**

> Bloqueado

2. **S4**

> Suspención/cliente suspendido

3. **PS**

> Mitad del proceso quedó en la parte tarjeta, nip o cvv

4. **CN**

> Cancelado por negocio

5. **C4**

> Cancelado por cliente

6. **a1**

> Activo (se contrato sin problemas)

7. **NE**

> No enrolado o nuevo paquete

8. **PA**

> Pendiente de activación

## Definiciones

1. Centro/URLScheme (dependiento si es token físico o token dijital en ese orden)

> App con la que te enrolaste

2. OTP

> Número de 8 dígitos que te genera el token para hacer transacciones

3. IUM

> Hash o cadena de caracteres que permite tener una sesión activa para ejecutar servicios al Back-End de Bancomer.

4. Seed

> Usada para calcular/crear el IUM, necesaria para hacer una contratación o una reactivación, si existe hay el keychain es VALIDO

4. Contratación 2x1

> Canal (canal móvil) y Token

5. Payload

> Datos necesarios de un cliente (telefono, ium) para hacer login, iniciar sesión y obtener un ticket que nos permitira consumir servicios del Back.end

## Arquitectura Verticales

<p align="justify">
	La aplicación vertical si es la aplicación CENTRO, cuenta con una base de datos que genera OTPs conocida como Token digital, así mismo, esta aplicación esta conectada a un .plist conocido como keychain el cual permite compartir información entre las demás apps del mismo proveedor de manera segura.
</p>

<p align="justify">
	Cuando una vertical genera una operación necesita una OTP, pidiendole permiso a la aplicación CENTRO la cual contiene el TOKEN así la aplicación centro es notificada y escribe la OTP en el KEYCHAIN posteriormente retorna el control a la VERTICAL la cual puede leer la OTP del KEYCHAIN y generar la operación o transacción.
</p>

## Canal

<p align="justify">
	Medio desde el que te conectas
</p>

<p align="justify">
	Actualmente existe la activación 2x1 es decir, te activa la banca móvil y el Token.
</p>

## Keychain

<p align="justify">
	Keychain es un .plist que se comparte entre aplicaciones de la misma cuenta.
</p>

<p align="justify">
	En el CONNECT un KEYCHAIN es valido cuando su <b>semilla</b> su <b>número</b> su <b>IUM</b> su <b>center/urlscheme</b> son válidos. Es decir, está enrolado correctamente enrolado.
</p>

## Token Físico

<p align="justify">
	Dispositivo físico asociado a un usuario que genera OTPs
</p>

1. T6

2. T3

<p align="justify">
	Keychain se define como <b>center</b>
</p>

> Físico 

## Token Digital

<p align="justify">
	Una base de datos interna que simula un TOKEN FÍSICO y que genera OTPs (claves de 8 dígitos) para hacer operaciones interbancarias.
</p>

1. S1

> Gemalto

2. S2

> Corporativo

3. T7

> Cronto

<p align="justify">
	Keychain se define como <b>urlscheme</b>
</p>

## Iconos

<p align="justify">
	Pueden obtener de <i>Enroll_IOS_Plantilla_IVR</i>	
</p>

## Api Mantenimiento

<p align="justify">
	Desbloqueo de contraseña
</p>

## Connect IVR 

1. DidEnterBackGround

> popToRootView

2. Ocultar boton Token

> Esperando llamada entra en Background

> viewWillAppear modifica la Bandera isWaitingIVRCall y qué el KeyChain es válido

3. ValidatingOperationOutSide

> Método encargado de abrir urls scheme

4. Post

> getState
> enroll state
> allowed

5. Login Propio

> Borrar banderas

### Activación de token

<p align="justify">
	Terminar el proceso de ingreso de datos y antes de recibir la llamada activa en el keychain: tel, seed, urlscheme
</p>

## Tips and Tricks

1. Al iniciar la aplicación en el método didFinishWithLaunch siempre settear la variable Server.getInstance sino aun que exista el Token las apis al no encontrar ese campo setteado desplegaran que no existe el token.





















